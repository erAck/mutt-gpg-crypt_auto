#!/bin/sh
# ~/.mutt/generate_crypt_auto
# Generate mutt crypt_auto* send-hooks from gpg pubring.
# Redirect output to file and source that in muttrc.
# Add the global hook _before_ sourcing the list:
# send-hook . 'reset crypt_autoencrypt'
# send-hook . 'set autocrypt_reply=yes'
# -=*# created by erAck #*=- CopyLeft Eike Rathke 2008-01-08/2019-08-14

# At least in an UTF-8 environment sed gets confused by 8-bit characters in
# real names and doesn't match the address anymore, an empty LANG variable
# works around.
LANG=

# https://git.gnupg.org/cgi-bin/gitweb.cgi?p=gnupg.git;a=blob;f=doc/DETAILS

# 2nd gpg colon field:
# d := disabled (deprecated - use the 'D' in field 12 instead)
# e := expired
# i := invalid (e.g. due to a missing self-signature)
# r := revoked

# Note that the following lines are part of the sed script passed by the shell
# and may not contain the ' character! Hence the double backslash in mail
# addresses to escape the regex . dot meta character for Mutt.
gpg --list-keys --with-colons --fixed-list-mode --no-secmem-warning | sed -ne '

:START

# ignore d|e|i|r keys
/^pub:[deir]:/ b IGNORE

# ignore disabled keys, D in 12th field
/^pub:\([^:]*:\)\{10\}[^:]*D/ b IGNORE

# take keys with encryption capability (E in 12th field), ignore without and
# other records like ^tru: or ^fpr:
# NOTE: this throws away also keys that have encryption capability only in their subkeys!
# there are a few..
# To solve that the entire key would have to be read first, which would not be sed ...
/^pub:\([^:]*:\)\{10\}[^:]*E/ ! b IGNORE

# extract uids and convert address to mutt hook and print
:EXTRACT
# ignore non-uid or no address
/^uid:[^der]:[^<]*<\([^:>]\+@[^:>]\+\)>/ ! b NUSKIP
# extract address
s/^uid:[^der]:[^<]*<\([^:>]\+@[^:>]\+\)>.*/\1/
# escape dot meta characters, with escaped backslash for mutt
s/\./\\\\./g
# print hook
s/\(.*\)/send-hook "!~l ~t \1" "set crypt_autoencrypt crypt_autosign autocrypt_reply=no"/p
:NUSKIP
n
/^pub:/ b START
b EXTRACT

# ignore entire key with uid/sub/... until next pub is encountered
:IGNORE
n
/^pub:/ b START
b IGNORE

' | \
    egrep -v 'WhatYouDontWantInThisList@example\\\\\.org' | \
    tr '[:upper:]' '[:lower:]' | \
    sort -u
# Note the triple escaped backslash!
