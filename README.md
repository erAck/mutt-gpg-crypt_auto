# mutt-gpg-crypt_auto

Generate mutt crypt_auto... send-hooks from gpg pubring.

## generate_crypt_auto

A shell script using *sed* to parse the output of
`gpg --list-keys --with-colons ...` and generate a list of Mutt

`send-hook "!~l ~t mail@example\\.org" "set crypt_autoencrypt crypt_autosign"`

commands for each UID, discarding disabled, expired, invalid and revoked keys.

The latest version of this script is also
[downloadable here](https://www.erack.de/download/)
as it was always since the year 2008 ...
